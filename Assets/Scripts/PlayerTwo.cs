﻿namespace Assets.Scripts
{
    class PlayerTwo : PlayerBase
    {
        public override string GetHorizontalAxies()
        {
            return "Horizontal2";
        }

        public override string GetJumpButton()
        {
            return "Jump2";
        }
    }
}
