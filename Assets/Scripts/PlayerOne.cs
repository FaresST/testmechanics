﻿public class PlayerOne : PlayerBase
{
    public override string GetHorizontalAxies()
    {
        return "Horizontal";
    }

    public override string GetJumpButton()
    {
        return "Jump";
    }
}
