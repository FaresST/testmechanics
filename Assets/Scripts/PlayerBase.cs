﻿using System.Collections;
using UnityEngine;

//[RequireComponent(typeof(Rigidbody))]
public abstract class PlayerBase : MonoBehaviour
{
    private Animator anim;
    private float moveSpeed = 4f;
    [SerializeField]
    private float jumpSpeed = 7f;
    //gravity for the falling WIP
    private float gravity = 20f;
    //rotation vars
    private Quaternion lookLeft;
    private Quaternion lookRight;
    private Rigidbody rb;
    private string horizontalAxies;
    private string jumpButton;
    private bool isIdel = false;
    [SerializeField]
    private bool isJummping = false;

    [SerializeField]
    private float velocityY;

    private bool isTouchingPlayer;
    private bool isTouchingGround;

    public PlayerBase()
    {
        horizontalAxies = GetHorizontalAxies();
        jumpButton = GetJumpButton();
    }

    public abstract string GetHorizontalAxies();
    public abstract string GetJumpButton();

    void Start()
    {

        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();

        //locking the rotation that so we can just replace the current rotation with the new rotations
        lookRight = transform.rotation;
        lookLeft = lookRight * Quaternion.Euler(0, -180, 0);

    }

    void FixedUpdate()
    {
        MoveThePlayer();

        Jump();

    }


    private void Jump()
    {
        if (Input.GetButton(jumpButton) && !isJummping)
        {
            isJummping = true;
            isIdel = false;

            anim.SetBool("Walk", false);

            anim.SetTrigger("Jump Inplace");

            var jumbspeed = 10;
            rb.velocity = new Vector3(0, jumbspeed, 0);
            //rb.AddForce(Vector2.up * jumbspeed);
            //rb.AddForce(Vector3.up * jumpSpeed);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {

        if (isTheColliderGroundBelowMe(collision))
        {
            isTouchingGround = true;

        }
        if (isTheColliderPlayerBelowMe(collision))
        {
            isTouchingPlayer = true;
        }

        isJummping = isTouchingGround || isTouchingPlayer ? false : true;
    }



    private bool isTheColliderGroundBelowMe(Collision collision)
    {
        var diffrentBetweenPlayerAndOther = transform.position.y - collision.transform.position.y;

        return (collision.gameObject.layer == 8 && diffrentBetweenPlayerAndOther >= 0) || transform.position.y < 0;
    }

    private bool isTheColliderPlayerBelowMe(Collision collision)
    {
        var diffrentBetweenPlayerAndOther = transform.position.y - collision.transform.position.y;
        return collision.gameObject.layer == 9 && diffrentBetweenPlayerAndOther >= 0.01871281;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            isTouchingGround = false;

        }
        if (collision.gameObject.layer == 9)
        {
            isTouchingPlayer = false;
        }

        isJummping = isTouchingGround || isTouchingPlayer ? false : true;
    }

    private void MoveThePlayer()
    {
        Vector3 movement = new Vector3(Input.GetAxis(horizontalAxies), 0f, 0f);
        if (movement.x > 0)
        {
            isIdel = false;
            transform.rotation = lookRight;

            if (!isJummping)
            {
                anim.SetBool("Walk", true);


            }
        }
        else if (movement.x < 0)
        {
            isIdel = false;
            transform.rotation = lookLeft;
            if (!isJummping)
            {
                anim.SetBool("Walk", true);


            }


        }
        else if (!isIdel)
        {
            isIdel = true;
            anim.SetBool("Walk", false);

            anim.SetTrigger("Idle");

            //StartCoroutine(Idling());
        }

        var x = Input.GetAxis(horizontalAxies);
        if (x != 0 && !isJummping)
        {
            rb.velocity = new Vector3(x * 8, 0, 0);
        }
        else if (movement.x != 0)
        {

            transform.position += movement * Time.fixedDeltaTime * 7f;
        }

    }

    IEnumerator Idling()
    {
        yield return new WaitForSeconds(3);
        anim.SetTrigger("Idle2");
    }

}
